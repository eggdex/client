import Swal, { SweetAlertIcon } from "sweetalert2";

declare global {
    interface Window {
        swal: (icon: SweetAlertIcon, message: string) => void;
    }
}