import { Contract } from 'ethers';
import { Listener, Provider } from 'ethers/providers';

export default class ContractMonitor {
    readonly contract: Contract;
    readonly provider: Provider;

    private listeners: { event: string; callback: Listener }[] = [];
    private providerListeners: { event: string; callback: Listener }[] = [];

    constructor(contract: Contract, provider: Provider) {
        this.contract = contract;
        this.provider = provider
    }

    contractListen(event: string, callback: Listener) {
        this.contract.on(event, callback);
        this.listeners.push({ event, callback });
    }

    providerListen(event: string, callback: Listener) {
        this.provider.on(event, callback);
        this.providerListeners.push({ event, callback });
    }

    clear() {
        for (const a of this.listeners) {
            this.contract.removeListener(a.event, a.callback);
        }

        for (const a of this.providerListeners) {
            this.provider.removeListener(a.event, a.callback);
        }

        this.listeners = [];
        this.providerListeners = [];
    }
}